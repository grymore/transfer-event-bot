const Moralis = require('moralis/node');
const fs = require('fs');
const axios = require('axios');

const env = 'dev'; // prod or dev

// LOCAL
// const bToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjMyOTA3OTU2LCJleHAiOjE2MzU0OTk5NTZ9.6y1Yj-t3D3FEVb8-qbx9rV6bHN6mDoT3B1pjYNcbGJI";
// const hostName = "http://localhost:1337";

// PRODUCTION
const bToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjMzMDU2MzkzLCJleHAiOjE2MzU2NDgzOTN9.BKdrbnGIZArKsNaLyfCUjIsA7wA2ob8nJ7SuSWaNxjk";
const hostName = "https://nefti.id";

const config = require('./config')[env];
const ABI_MP = [{"inputs":[{"internalType":"address","name":"_NEFTi20","type":"address"},{"internalType":"address","name":"_NEFTiMT","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"_sid","type":"uint256"},{"indexed":false,"internalType":"address","name":"_negotiator","type":"address"}],"name":"BidCanceled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"saleId","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"},{"indexed":true,"internalType":"address","name":"seller","type":"address"},{"indexed":false,"internalType":"uint8","name":"status","type":"uint8"}],"name":"CancelSale","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"saleId","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"price","type":"uint256"},{"indexed":true,"internalType":"address","name":"negotiator","type":"address"},{"indexed":false,"internalType":"uint256","name":"negoDate","type":"uint256"},{"indexed":false,"internalType":"uint8","name":"status","type":"uint8"}],"name":"Negotiate","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"uint256","name":"_sid","type":"uint256"},{"indexed":false,"internalType":"address","name":"_negotiator","type":"address"}],"name":"NegotiationCanceled","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"purchaseId","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"saleId","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"price","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint8","name":"saleMethod","type":"uint8"},{"indexed":false,"internalType":"address","name":"seller","type":"address"},{"indexed":false,"internalType":"bool[4]","name":"states","type":"bool[4]"},{"indexed":false,"internalType":"uint8","name":"status","type":"uint8"}],"name":"Purchase","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"uint256","name":"saleId","type":"uint256"},{"indexed":true,"internalType":"uint256","name":"tokenId","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"price","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint8","name":"saleMethod","type":"uint8"},{"indexed":true,"internalType":"address","name":"seller","type":"address"},{"indexed":false,"internalType":"bool[4]","name":"states","type":"bool[4]"},{"indexed":false,"internalType":"uint256[2]","name":"saleDate","type":"uint256[2]"},{"indexed":false,"internalType":"uint8","name":"status","type":"uint8"}],"name":"Sale","type":"event"},{"stateMutability":"payable","type":"fallback"},{"inputs":[{"internalType":"address","name":"_seller","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"address","name":"_bidder","type":"address"}],"name":"cancelAuctionBid","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"address","name":"_negotiator","type":"address"}],"name":"cancelNegotiation","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"cancelSaleItem","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"getAuctionBidders","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"address","name":"_bidder","type":"address"}],"name":"getBidValue","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"getHighestBidValue","outputs":[{"internalType":"address","name":"bidder","type":"address"},{"internalType":"uint256","name":"bid","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"getListingCancellationFee","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"address","name":"_negotiator","type":"address"}],"name":"getNegotiationInfo","outputs":[{"internalType":"uint256","name":"saleId","type":"uint256"},{"internalType":"uint256","name":"value","type":"uint256"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"negoDate","type":"uint256"},{"internalType":"uint8","name":"status","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"getNegotiators","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"getSaleItemsInfo","outputs":[{"internalType":"uint256[3]","name":"info","type":"uint256[3]"},{"internalType":"address","name":"seller","type":"address"},{"internalType":"bool[4]","name":"states","type":"bool[4]"},{"internalType":"uint256[2]","name":"saleDate","type":"uint256[2]"},{"internalType":"uint256[3]","name":"values","type":"uint256[3]"},{"internalType":"address","name":"buyer","type":"address"},{"internalType":"uint8","name":"status","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_seller","type":"address"}],"name":"itemsOf","outputs":[{"internalType":"uint256[]","name":"items","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"bytes","name":"","type":"bytes"}],"name":"onERC1155Received","outputs":[{"internalType":"bytes4","name":"","type":"bytes4"}],"stateMutability":"pure","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"}],"name":"txAcceptAuctionBid","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"address","name":"_negotiator","type":"address"}],"name":"txAcceptContract","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"address","name":"_negotiator","type":"address"}],"name":"txAcceptDirectOffering","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_price","type":"uint256"}],"name":"txBid","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"uint256","name":"_price","type":"uint256"},{"internalType":"uint256[2]","name":"_dates","type":"uint256[2]"}],"name":"txContractOffering","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_pid","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"txDirectBuy","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"uint256","name":"_price","type":"uint256"}],"name":"txDirectOffering","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_sid","type":"uint256"},{"internalType":"uint256","name":"_tokenId","type":"uint256"},{"internalType":"uint256","name":"_price","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"uint8","name":"_saleMethod","type":"uint8"},{"internalType":"bool[4]","name":"_states","type":"bool[4]"},{"internalType":"uint256[2]","name":"_saleDate","type":"uint256[2]"}],"name":"txSaleItems","outputs":[],"stateMutability":"nonpayable","type":"function"},{"stateMutability":"payable","type":"receive"}];
const ABI_NFT = [{"inputs":[{"internalType":"string","name":"name_","type":"string"},{"internalType":"string","name":"symbol_","type":"string"},{"internalType":"string","name":"baseMetadataURI_","type":"string"},{"internalType":"address","name":"calcFeeExt_","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_owner","type":"address"},{"indexed":true,"internalType":"address","name":"_operator","type":"address"},{"indexed":false,"internalType":"bool","name":"_approved","type":"bool"}],"name":"ApprovalForAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"signer","type":"address"},{"indexed":false,"internalType":"uint256","name":"newNonce","type":"uint256"}],"name":"NonceChange","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"asAddress","type":"address"},{"indexed":true,"internalType":"uint256","name":"asUInt","type":"uint256"}],"name":"RequestId","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_operator","type":"address"},{"indexed":true,"internalType":"address","name":"_from","type":"address"},{"indexed":true,"internalType":"address","name":"_to","type":"address"},{"indexed":false,"internalType":"uint256[]","name":"_ids","type":"uint256[]"},{"indexed":false,"internalType":"uint256[]","name":"_amounts","type":"uint256[]"}],"name":"TransferBatch","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"_operator","type":"address"},{"indexed":true,"internalType":"address","name":"_from","type":"address"},{"indexed":true,"internalType":"address","name":"_to","type":"address"},{"indexed":false,"internalType":"uint256","name":"_id","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"_amount","type":"uint256"}],"name":"TransferSingle","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"string","name":"_uri","type":"string"},{"indexed":true,"internalType":"uint256","name":"_id","type":"uint256"}],"name":"URI","type":"event"},{"stateMutability":"payable","type":"fallback"},{"inputs":[{"internalType":"address","name":"_owner","type":"address"},{"internalType":"uint256","name":"_id","type":"uint256"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address[]","name":"_owners","type":"address[]"},{"internalType":"uint256[]","name":"_ids","type":"uint256[]"}],"name":"balanceOfBatch","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"uint256[]","name":"_ids","type":"uint256[]"},{"internalType":"uint256[]","name":"_values","type":"uint256[]"}],"name":"batchBurn","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256[]","name":"_ids","type":"uint256[]"},{"internalType":"uint256[]","name":"_values","type":"uint256[]"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"batchMint","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"uint256","name":"_id","type":"uint256"},{"internalType":"uint256","name":"_value","type":"uint256"}],"name":"burn","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256[]","name":"_amounts","type":"uint256[]"}],"name":"getBatchMintFee","outputs":[{"internalType":"uint256[3]","name":"mintFee","type":"uint256[3]"},{"internalType":"uint256","name":"multitokenOnEach","type":"uint256"},{"internalType":"string","name":"feeAs","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getDefaultFeeType","outputs":[{"internalType":"uint8","name":"feeType","type":"uint8"},{"internalType":"string","name":"feeTypeAsString","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_id","type":"uint256"}],"name":"getIDBinIndex","outputs":[{"internalType":"uint256","name":"bin","type":"uint256"},{"internalType":"uint256","name":"index","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"getMintFee","outputs":[{"internalType":"uint256[3]","name":"mintFee","type":"uint256[3]"},{"internalType":"uint256","name":"multitokenOnEach","type":"uint256"},{"internalType":"string","name":"feeAs","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_signer","type":"address"}],"name":"getNonce","outputs":[{"internalType":"uint256","name":"nonce","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"_binValues","type":"uint256"},{"internalType":"uint256","name":"_index","type":"uint256"}],"name":"getValueInBin","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"pure","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"}],"name":"holders","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_owner","type":"address"},{"internalType":"address","name":"_operator","type":"address"}],"name":"isApprovedForAll","outputs":[{"internalType":"bool","name":"isOperator","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_signerAddress","type":"address"},{"internalType":"bytes32","name":"_hash","type":"bytes32"},{"internalType":"bytes","name":"_data","type":"bytes"},{"internalType":"bytes","name":"_sig","type":"bytes"}],"name":"isValidSignature","outputs":[{"internalType":"bool","name":"isValid","type":"bool"},{"internalType":"address","name":"signer","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_holder","type":"address"}],"name":"itemsOf","outputs":[{"internalType":"uint256[]","name":"","type":"uint256[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256[]","name":"_ids","type":"uint256[]"},{"internalType":"uint256[]","name":"_amounts","type":"uint256[]"},{"internalType":"bool","name":"_isGasFee","type":"bool"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"metaSafeBatchTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_id","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"bool","name":"_isGasFee","type":"bool"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"metaSafeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_owner","type":"address"},{"internalType":"address","name":"_operator","type":"address"},{"internalType":"bool","name":"_approved","type":"bool"},{"internalType":"bool","name":"_isGasFee","type":"bool"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"metaSetApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_id","type":"uint256"},{"internalType":"uint256","name":"_value","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"mint","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_id","type":"uint256"}],"name":"minterOf","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"requestId","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256[]","name":"_ids","type":"uint256[]"},{"internalType":"uint256[]","name":"_amounts","type":"uint256[]"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeBatchTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_from","type":"address"},{"internalType":"address","name":"_to","type":"address"},{"internalType":"uint256","name":"_id","type":"uint256"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"bytes","name":"_data","type":"bytes"}],"name":"safeTransferFrom","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_operator","type":"address"},{"internalType":"bool","name":"_approved","type":"bool"}],"name":"setApprovalForAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"string","name":"_newBaseMetadataURI","type":"string"}],"name":"setBaseMetadataURI","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"_newCalcFeeExt","type":"address"}],"name":"setCalcFeeExt","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"bytes4","name":"_interfaceID","type":"bytes4"}],"name":"supportsInterface","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_id","type":"uint256"}],"name":"uri","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"_signer","type":"address"},{"internalType":"uint256","name":"_tokenId","type":"uint256"},{"internalType":"bytes32","name":"_hash","type":"bytes32"},{"internalType":"bytes32","name":"_r","type":"bytes32"},{"internalType":"bytes32","name":"_s","type":"bytes32"},{"internalType":"uint8","name":"_v","type":"uint8"}],"name":"verifyOwnership","outputs":[{"internalType":"bool","name":"success","type":"bool"},{"internalType":"address","name":"signer","type":"address"}],"stateMutability":"view","type":"function"},{"stateMutability":"payable","type":"receive"}];

var currentBlockNumber = require('./config/lastBlockNumber.json').blockNumber;

(async function() {
  const init = async () => {
    const Web3 = Moralis.Web3;
    const web3 = new Web3(config.rpc);

    // let test = await web3.eth.getTransaction("0x2d224eccea682198fd580a0922085fd0edf601828098e59964b174756761147c", function (error, result){
    //     console.log(result.from.toLowerCase());
    // });

    // console.log(test.from.toLowerCase());

    const cMarketplace = await new web3.eth.Contract(ABI_MP, '0xdc29f6ed4d3c8f7ce7e9f5b8f69e130194d01c1e');
    const scNeftipedia1155 = await new web3.eth.Contract(ABI_NFT, '0x31727F0a0D659FC767069Fb461d8C53Ab7287387');
    
    let evOptions = {
        filter: {
            value: [],
        },
        fromBlock: currentBlockNumber  // -- load json when node start
    };

    scNeftipedia1155.events.TransferSingle(evOptions)
        .on('changed', changed  => console.log('(i) NFT1155_TransferSingle_onChanged ', changed))
        .on('error', err        => console.error('(!) NFT1155_TransferSingle_onError ? ', err))
        .on('connected', str    => console.log('(i) NFT1155_TransferSingle_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NFT1155_TransferSingle_onData ? ', event)
            
        })

    cMarketplace.events.BidCanceled(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_BidCanceled_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_BidCanceled_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_BidCanceled_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NEFTiMP_BidCanceled_onData ? ', event)
            // save block number to json file
            fs.writeFileSync('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2));

            var data = {
                "transactionHash": event.transactionHash,
                "sid": event.returnValues.sid,
                "negotiator": event.returnValues.negotiator
            }
  
            axios.post(
                hostName+'/bid-canceleds',
                data, 
                { 
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bToken,
                }
                }
            ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

        });
    
    cMarketplace.events.CancelSale(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_CancelSale_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_CancelSale_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_CancelSale_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NEFTiMP_CancelSale_onData ? ', event)
            // save block number to json file
            fs.writeFileSync('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2));

            var data = {
                "transactionHash": event.transactionHash,
                "saleId": event.returnValues.saleId,
                "tokenId": event.returnValues.tokenId,
                "seller": event.returnValues.seller,
                "status": event.returnValues.status,
            }
  
            axios.post(
                hostName+'/cancel-sales',
                data, 
                { 
                headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bToken,
                }
                }
            ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

        });
    
    cMarketplace.events.Negotiate(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_Negotiate_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_Negotiate_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_Negotiate_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NEFTiMP_Negotiate_onData ? ', event)
            // save block number to json file
            fs.writeFileSync('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2));

            var data = {
              "transactionHash": event.transactionHash,
              "saleId": event.returnValues.saleId,
              "tokenId": event.returnValues.tokenId,
              "amount": event.returnValues.amount,
              "price": event.returnValues.price,
              "negotiator": event.returnValues.negotiator,
              "negoDate": event.returnValues.negoDate,
              "status": event.returnValues.status
            }

            axios.post(
                hostName+'/negotiates',
                data, 
                { 
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bToken,
                  }
                }
            ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

        });

    cMarketplace.events.NegotiationCanceled(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_BidCanceled_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_BidCanceled_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_BidCanceled_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NEFTiMP_BidCanceled_onData ? ', event)
            // save block number to json file
            fs.writeFileSync('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2));

            var data = {
                "transactionHash": event.transactionHash,
                "sid": event.returnValues._sid,
                "negotiator": event.returnValues._negotiator
            }

            axios.post(
                hostName+'/negotiation-canceleds',
                data, 
                { 
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bToken,
                  }
                }
            ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

        });

    cMarketplace.events.OwnershipTransferred(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_OwnershipTransferred_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_OwnershipTransferred_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_OwnershipTransferred_onConnect ? ', str))
        .on('data', event       => {
            console.log('(i) NEFTiMP_OwnershipTransferred_onData ? ', event)
            // save block number to json file
            fs.writeFileSync('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2));

            var data = {
                "transactionHash": event.transactionHash,
                "purchaseId": event.returnValues.purchaseId,
                "previousOwner": event.returnValues.previousOwner,
                "newOwner": event.returnValues.newOwner,
            }

            axios.post(
                hostName+'/ownership-transferreds',
                data, 
                { 
                  headers: { 
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + bToken,
                  }
                }
            ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

        });

    cMarketplace.events.Purchase(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_Purchase_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_Purchase_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_Purchase_onConnect ? ', str))
        .on('data', event => {
            
            console.log('(i) NEFTiMP_Purchase_onData ? ', event)
            // save block number to json file
            fs.writeFile('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2), async () => {

                let owner = await web3.eth.getTransaction(event.transactionHash, function (error, result){
                    if(error) {console.log(error)}
                    console.log(result.from.toLowerCase());
                })

                let getMetadata = await axios.get(
                    hostName+'/metadata/'+event.returnValues.tokenId, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                );
                var metadataCon = getMetadata.data.metadataId.toLowerCase();
                console.log("Metadata ID : ", metadataCon);
                let metadata = {};
                try {
                    let rawdata = fs.readFileSync('/var/www/metadata/'+metadataCon+'/metadata.json');
                    metadata = JSON.parse(rawdata);
                    console.log(metadata);

                    var history = {
                        "method": "purchase",
                        "transactionHash": event.transactionHash,
                        "from": owner.from,
                        "to": owner.to,
                        "amount": event.returnValues.amount,
                        "status": event.returnValues.status,
                        "saleMethod": event.returnValues.saleMethod,
                        "metadata": metadata,
                    }
    
                    axios.post(
                        hostName+'/history-marketplaces',
                        history, 
                        {
                          headers: { 
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + bToken,
                          }
                        }
                    ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});
        
                }catch(e) {
                    console.log(e);
                }

                var data = {
                    "transactionHash": event.transactionHash,
                    "purchaseId": event.returnValues.purchaseId,
                    "saleId": event.returnValues.saleId,
                    "tokenId": event.returnValues.tokenId,
                    "price": event.returnValues.price,
                    "amount": event.returnValues.amount,
                    "saleMethod": event.returnValues.saleMethod,
                    "seller": event.returnValues.seller,
                    "isPostPaid": event.returnValues.states[0],
                    "isNegotiable": event.returnValues.states[1],
                    "isAuction": event.returnValues.states[2],
                    "isContract": event.returnValues.states[3],
                    "status": event.returnValues.status,
                    "from": owner.from,
                }

                axios.post(
                    hostName+'/purchases',
                    data, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});
    
                let realowner = await axios.get(
                    hostName+'/profiles/'+owner.from, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                );
                
                var product = {
                    "Type": "not_for_sale",
                    "owner": realowner.data.id,
                    "saleStatus": event.returnValues.status
                }
    
                axios.put(
                    hostName+'/marketplace-products/'+event.returnValues.tokenId,
                    product, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});
            });

            

        });

    cMarketplace.events.Sale(evOptions)
        .on('changed', changed  => console.log('(i) NEFTiMP_Sale_onChanged ', changed))
        .on('error', err        => console.error('(!) NEFTiMP_Sale_onError ? ', err))
        .on('connected', str    => console.log('(i) NEFTiMP_Sale_onConnect ? ', str))
        .on('data', event => {
            console.log('(i) NEFTiMP_Sale_onData ? ', event)

            // save block number to json file
            fs.writeFile('./config/lastBlockNumber.json', JSON.stringify({
                blockNumber: event.blockNumber + 1
            }, null, 2), async () => {

                let owner = await web3.eth.getTransaction(event.transactionHash, function (error, result){
                    if(error) {console.log(error)}
                    console.log(result.from.toLowerCase());
                });
                
                var data = {
                    "transactionHash": event.transactionHash,
                    "saleId": event.returnValues.saleId,
                    "tokenId": event.returnValues.tokenId,
                    "price": event.returnValues.price,
                    "amount": event.returnValues.amount,
                    "saleMethod": event.returnValues.saleMethod,
                    "seller": event.returnValues.seller,
                    "isPostPaid": event.returnValues.states[0],
                    "isNegotiable": event.returnValues.states[1],
                    "isAuction": event.returnValues.states[2],
                    "isContract": event.returnValues.states[3],
                    "startDate": event.returnValues.saleDate[0],
                    "endDate": event.returnValues.saleDate[1],
                    "status": event.returnValues.status
                }
    
                var product = {
                    "saleItemId": event.returnValues.saleId,
                    "saleStatus": event.returnValues.status
                }
                
                let getMetadata = await axios.get(
                    hostName+'/metadata/'+event.returnValues.tokenId, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                );
                var metadataCon = getMetadata.data.metadataId.toLowerCase();
                console.log("Metadata ID : ", metadataCon);
                let metadata = {};
                try {
                    let rawdata = fs.readFileSync('/var/www/metadata/'+metadataCon+'/metadata.json');
                    metadata = JSON.parse(rawdata);
                    console.log(metadata);

                    var history = {
                        "method": "sale",
                        "transactionHash": event.transactionHash,
                        "from": owner.from,
                        "to": owner.to,
                        "amount": event.returnValues.amount,
                        "status": event.returnValues.status,
                        "saleMethod": event.returnValues.saleMethod,
                        "metadata": metadata,
                    }
    
                    axios.post(
                        hostName+'/history-marketplaces',
                        history, 
                        {
                          headers: { 
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer ' + bToken,
                          }
                        }
                    ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});
        
                }catch(e) {
                    console.log(e);
                }
    
                
                axios.post(
                    hostName+'/sales',
                    data, 
                    { 
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});
    
                axios.put(
                    hostName+'/marketplace-products/'+event.returnValues.tokenId,
                    product, 
                    {
                      headers: { 
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + bToken,
                      }
                    }
                ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

            });

            

        });     
    }

  init();
  
}());