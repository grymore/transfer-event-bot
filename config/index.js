module.exports = {
  dev: {
    // rpc: 'https://data-seed-prebsc-1-s1.binance.org:8545/',
    // rpc: 'https://data-seed-prebsc-2-s1.binance.org:8545/',
    // rpc: 'https://data-seed-prebsc-1-s2.binance.org:8545/',
    // rpc: 'https://data-seed-prebsc-2-s2.binance.org:8545/',
    // rpc: 'https://data-seed-prebsc-1-s3.binance.org:8545/',
    // rpc: 'https://data-seed-prebsc-2-s3.binance.org:8545/',
    // rpc: 'https://speedy-nodes-nyc.moralis.io/ed5a84c223ee04ddaea7062e/bsc/testnet',
    rpc: 'wss://speedy-nodes-nyc.moralis.io/ed5a84c223ee04ddaea7062e/bsc/testnet/ws',
    networkId: 97,
    chainId: 97,
    listenTo: {
      address: '0x604bd5d642f5e4e7d3a51bb7d614db233488a479',
    }
  },
  prod: {
    // rpc: 'https://bsc-dataseed.binance.org/',
    // rpc: 'https://bsc-dataseed1.defibit.io/',
    // rpc: 'https://bsc-dataseed1.ninicoin.io/',
    // rpc: 'https://speedy-nodes-nyc.moralis.io/ed5a84c223ee04ddaea7062e/bsc/mainnet',
    rpc: 'wss://speedy-nodes-nyc.moralis.io/2b3ce1a1bf197cbf2aa41957/bsc/mainnet/ws',
    networkId: 56,
    chainId: 56,
    listenTo: {
      // address: '0xDF3b6a8bF27e2c1F6fDc4193e86a95EC8741aF39',
      address: '0x0A194EDb637c0C3007387a8Bf821C5C7C8274642',
    },
    listenFor: [
      {
        name: 'DOOiT',
        symbol: 'DOO',
        // contract: '0x8e87DB40C5E9335a8FE19333Ffc19AD95C665f60',
        contract: '0x8e87db40c5e9335a8fe19333ffc19ad95c665f60',
        abi: require('./abi/BEP20ReadOnly.json'),
        type: 'token'
      },
      {
        name: 'Binance-Peg USD Coin (USDC)',
        symbol: 'USDC',
        // contract: '0x8AC76a51cc950d9822D68b83fE1Ad97B32Cd580d',
        contract: '0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d',
        abi: require('./abi/BEP20ReadOnly.json'),
        type: 'token'
      },
      {
        name: 'Binance-Peg BSC-USD (BSC-USD)',
        symbol: 'USDT',
        // contract: '0x55d398326f99059fF775485246999027B3197955',
        contract: '0x55d398326f99059ff775485246999027b3197955',
        abi: require('./abi/BEP20ReadOnly.json'),
        type: 'token'
      },
      {
        name: 'Binance-Peg BUSD Token (BUSD)',
        symbol: 'BUSD',
        // contract: '0xe9e7CEA3DedcA5984780Bafc599bD69ADd087D56',
        contract: '0xe9e7cea3dedca5984780bafc599bd69add087d56',
        abi: require('./abi/BEP20ReadOnly.json'),
        type: 'token'
      },
    ]
  }
};