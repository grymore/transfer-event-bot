const axios = require('axios');
const Moralis = require('moralis/node');
const fs = require('fs');

const env = 'prod'; // prod or dev

const bToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjMyODg5OTQ5LCJleHAiOjE2MzU0ODE5NDl9.8THi9Ui410yyGZuzxg7JF1sUlGPF5yORRjcQiKlOfR0";
const config = require('./config')[env];
const bulkInsert = require('./config/listener/dooit.json').bulkInsert;

(async function() {
  const init = async () => {
    const Web3 = Moralis.Web3;
    const web3 = new Web3(config.rpc);

    console.log(`\n\n`);
    console.log(`============================================================`);
    console.log(`@ Web3 Starting..`);
    console.log('  Node Version : ', web3.version);
    console.log('  Node Info    : ', await web3.eth.getNodeInfo());
    console.log('  Network      : ', await web3.eth.getChainId());
    console.log('  Listening    : ', await web3.eth.net.isListening());
    console.log('  Peer Count   : ', await web3.eth.net.getPeerCount());
    console.log(`\n--- Monitoring ---\n`);

    Moralis.initialize("8w9oXwO9dKPSQJnu0Tor2mkSkCDQCIUIxA3bWcsy", "", "Vl6QEP79uwkiRTRRryEZtIcZ0SvopuuJyPrLxJgU");
    Moralis.serverURL = 'https://4gsnhv4eyvcd.moralis.io:2053/server';

    // create query
    const Transfer = new Moralis.Query('DooitTransfer');

    // subscribe for real-time updates
    const subsTransfer = await Transfer.subscribe();
    subsTransfer.on('create', async function(data) {
      //console.log(data);
      proceedToDatabase(data);
    });
    
    const proceedToDatabase = async (data) => {
      console.log(data);
      
      try {
        let amount = data.get('value').concat("00");
        let jsonData = {
          token_address: data.get('address'),
          from: data.get('from'), 
          to: data.get('to'),
          amount: (parseFloat(web3.utils.fromWei(amount, 'ether'))).toFixed(6),
          transaction_hash: data.get('transaction_hash'),
          block_number: data.get('block_number'),
          block_timestamp: data.get('block_timestamp') 
        };

        await axios.post(
          'https://nefti.id/history-transactions',
          jsonData, 
          { 
            headers: { 
              'Content-Type': 'application/json',
              'Authorization': 'Bearer ' + bToken,
            }
          }
        ).then((response) => { console.log(response.data);}).catch((e) => { console.log(e)});

      } catch(e) {
        console.log(e);
      }
    };

    if(bulkInsert == true){
      const AllTransfer = Moralis.Object.extend("DooitTransfer");
      const query = new Moralis.Query(AllTransfer);
      const count = await query.count();
      console.log(count);
      let skip = 100;
      let index = 0;
      for (let i = 0; i < (count/100); i++) {
        query.limit(100);
        query.skip(skip);
        const results = await query.find();
        skip += 100;
        await sleep(10000)
        function sleep(ms) {
          return new Promise((resolve) => {
            setTimeout(resolve, ms);
          });
        }
        for (let k = 0; k < results.length; k++) {
          index = ((i+1) * 100 - 100) + (k+1);
          console.log(index);
          const object = results[k];
          
          proceedToDatabase(object);
        }
      }
      fs.writeFileSync('./config/listener/dooit.json', JSON.stringify({
        bulkInsert: false
      }, null, 2));
    }
  };

  await init();

}());
